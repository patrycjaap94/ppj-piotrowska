import java.util.*;
import java.lang.*;
import java.time.*;
import pl.imiajd.piotrowskapatrycja.*;

public class TestOsoba
{
        public static void main(String[] args)
        {
            ArrayList<Osoba> grupa = new ArrayList<>();
            grupa.add(new Osoba("Patrycja",1994,9,3));
            grupa.add(new Osoba("Patrycja",1994,9,12));
            grupa.add(new Osoba("Adam",1992,12,12));
            grupa.add(new Osoba("Zofia",1956,1,14));
            grupa.add(new Osoba("Krzys",1678,12,19));
            System.out.println(grupa);
            Collections.sort(grupa);
            System.out.println(grupa);
        }
}
