import java.util.*;
import java.lang.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TestPliki
{
        public static void main(String[] args)
        {
            if (args.length != 1) {
                System.err.println("Sposób: java TestPliki NAZWA_PLIKU");
                System.exit(1);
            }
            ArrayList<String> lista=new ArrayList<>();

            try {
                Scanner file = new Scanner(new File(args[0]));
                while (file.hasNextLine()) {
                    lista.add(file.nextLine());
                    Collections.sort(lista);
                }
            }

            catch (FileNotFoundException e) {
                System.err.println("FileNotFoundException: " + e.getMessage());
                System.exit(1);
            } catch (Exception e) {
                System.err.println("Caught Exception: " + e.getMessage());
                System.exit(2);
            }
            finally {
             for(String i : lista){
				 System.out.println(i);
				 }
            }
        }

}
