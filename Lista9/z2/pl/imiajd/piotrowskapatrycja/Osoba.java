package pl.imiajd.piotrowskapatrycja;
import java.util.*;
import java.lang.*;
import java.time.*;

public class Osoba implements Comparable<Osoba>,Cloneable {

        public Osoba(String nazwisko,int rok,int miesiac,int dzien) {
            this.nazwisko=nazwisko;
            dataUrodzenia=LocalDate.of(rok,miesiac,dzien);
        }

        public String toString() {

            return "["+nazwisko.toString()+"|"+dataUrodzenia.toString()+"]";

        }
        public Osoba clone() throws CloneNotSupportedException
        {
            Osoba cloned = (Osoba) super.clone();
            return cloned;
        }

        public boolean equals(Osoba p) {
            if (this == p) {
                return true;
            }

            if(p==null) {
                return false;
            }

            if(getClass()!= p.getClass()) {
                return false;
            }
            if( !(p instanceof Osoba)) {
                return false;
            }

            if(this.nazwisko.equals(p.nazwisko)) {
                if(this.dataUrodzenia.equals(p.dataUrodzenia)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        public int compareTo(Osoba p) {


            if(this.nazwisko.compareTo(p.nazwisko) == 0 ) {
                return this.dataUrodzenia.compareTo(p.dataUrodzenia);
            }
            else {
                return this.nazwisko.compareTo(p.nazwisko);

            }
        }

        private String nazwisko;
        private LocalDate dataUrodzenia;
}
