package pl.imiajd.piotrowskapatrycja;
import java.util.*;
import java.time.*;
import java.lang.*;


public class Student extends Osoba {

        public Student(String nazwisko,int rok,int miesiac,int dzien,double sredniaOcen) {
            super(nazwisko,rok,miesiac,dzien);
            this.sredniaOcen=sredniaOcen;
        }
        public String toString() {
            return "["+super.toString()+"|"+Double.toString(sredniaOcen)+"]";
        }
        public Student clone() throws CloneNotSupportedException
        {Student cloned = (Student) super.clone();
         cloned.sredniaOcen=this.sredniaOcen;
            return cloned;
        }
        public boolean equals(Student p) {
            if (this == p) {
                return true;
            }

            if(p==null) {
                return false;
            }

            if(getClass()!= p.getClass()) {
                return false;
            }
            if( !(p instanceof Osoba)) {
                return false;
            }
            if(((Osoba)this).equals(((Osoba)p))) {
                if(sredniaOcen==p.sredniaOcen) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        public int compareTo(Student p) {

            if(((Osoba) this).compareTo(((Osoba)p)) == 0 ) {
				double x=this.sredniaOcen-p.sredniaOcen;
                return (int)x;
            }
            else {
                return ((Osoba)this).compareTo(((Osoba)p));
            }
        }
        private double sredniaOcen;
}
