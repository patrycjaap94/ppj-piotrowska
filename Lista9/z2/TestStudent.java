import java.util.*;
import java.lang.*;
import java.time.*;
import pl.imiajd.piotrowskapatrycja.*;

public class TestStudent
{
        public static void main(String[] args)
        {
            ArrayList<Student> grupa = new ArrayList<>();
            grupa.add(new Student("Patrycja",1994,2,3,5.01));
            grupa.add(new Student("Adam",1993,11,11,1.2));
            grupa.add(new Student("Tadeusz",1956,12,12,4.2));
            grupa.add(new Student("Kaludia",19934,11,13,3.2));
            grupa.add(new Student("Stasiu",2015,11,25,5.4));
            System.out.println(grupa);
            Collections.sort(grupa);
            System.out.println(grupa);
            Student s1= new Student("Kazimiera",1993,1,2,5);
            try {
                Student s1_kopia=s1.clone();
                System.out.println(s1_kopia);
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            ;
        }
}
