package pl.imiajd.piotrowskapatrycja;
import java.util.*;
import java.time.*;
public class Student extends Osoba
{
    public Student(String nazwisko,String[] imiona,boolean plec,int rok,int miesiac,int dzien, String kierunek,double sredniaOcen)
    {
    super(nazwisko,imiona,plec,rok,miesiac,dzien);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {return "kierunek: " + kierunek;}
        public double getSrednia()
    {return sredniaOcen; }
   
    public void setSrednia(double srednia)
	{sredniaOcen=srednia;
		}
    private String kierunek;
    private double sredniaOcen;
}

