package pl.imiajd.piotrowskapatrycja;
import java.util.*;
import java.time.*;
public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,String[] imiona,boolean plec,int rok,int miesiac,int dzien,double pobory,int rokz,int miesiacz,int dzienz)
    {super(nazwisko,imiona,plec,rok,miesiac,dzien);
       
        this.dataZatrudnienia=LocalDate.of(rokz,miesiacz,dzienz); 
		this.pobory = pobory;
    }

    public double getPobory()
    { return pobory;}
        public LocalDate getdataZatrudnienia()
    {return dataZatrudnienia;}

    public String getOpis()
    {return String.format("Pracownik otrzymuje pensje %.2f złotych", pobory);}

   
    private LocalDate dataZatrudnienia; 
	private double pobory;
}
