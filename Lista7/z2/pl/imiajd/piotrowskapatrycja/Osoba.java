package pl.imiajd.piotrowskapatrycja;
import java.util.*;
import java.time.*;

public abstract class Osoba
{
    public Osoba(String nazwisko,String[] imiona,boolean plec,int rok,int miesiac,int dzien)
    {
        this.nazwisko = nazwisko;
        this.imiona=new String[2];
        this.imiona[0]=imiona[0];
        this.imiona[1]=imiona[1];
		
        this.dataUrodzenia=LocalDate.of(rok,miesiac,dzien);
		
			this.plec=plec;
        
        
        
    }

	
	
	
    public abstract String getOpis();

    public String getNazwisko()
    { return nazwisko;}
	
        public String[] getImiona()
    { return imiona;}
        public LocalDate getdataUrodzenia()
    { return dataUrodzenia;}
        public boolean getPlec()
    { return plec;}

    private String nazwisko;
    private String[] imiona;
    private boolean plec;    
	private LocalDate dataUrodzenia;
}
