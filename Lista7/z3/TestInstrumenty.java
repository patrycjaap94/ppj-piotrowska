import pl.imiajd.piotrowska.*;
import java.time.*;
import java.util.*;
public class TestInstrumenty
{
    public static void main(String[] args)
    {
		ArrayList<Instrument> orkiestra = new ArrayList<Instrument>();

        orkiestra.add (new Flet("ASD", 2014, 02,11));
        orkiestra.add (new Fortepian("zcvbg", 1215, 03,14));
        orkiestra.add (new Skrzypce("qwero", 1994, 02,16));
        orkiestra.add (new Flet("yytry", 1456, 09,11));
        orkiestra.add (new Fortepian("jkgr", 2002, 03,03));
        
        for (Instrument i : orkiestra) {
			if (i.getClass()==Flet.class)
			{  System.out.println (((Flet)i).toString());
				System.out.println (((Flet)i).dzwiek());
			}
			if (i.getClass()==Fortepian.class)
			{System.out.println (((Fortepian)i).toString());
			System.out.println (((Fortepian)i).dzwiek());
			}
			if (i.getClass()==Skrzypce.class)
			{System.out.println (((Skrzypce)i).toString());
			System.out.println (((Skrzypce)i).dzwiek());
			}
        }
        
    }
	}

