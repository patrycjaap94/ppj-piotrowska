package pl.imiajd.piotrowska;
import java.util.*;
import java.time.*;
public class Skrzypce extends Instrument
{
	public Skrzypce(String producent, int y, int m, int d)
	{super(producent,y,m,d);}
	
	public String dzwiek()
	{return ("brzeczenie skrzypiec");}
	
	public String toString()
	{return super.toString();}
	
	public boolean equals (Object a)
	{return super.equals(a);}
}

