package pl.imiajd.piotrowska;
import java.util.*;
import java.time.*;

public abstract class Instrument
{
    public Instrument(String producent, int y, int m, int d)
    {
        this.producent = producent;
		this.rokProdukcji =  rokProdukcji.of(y,m,d);		
    }
    public String getProducent()
    { return producent;}
	
	public LocalDate getRokProdukcji()
    { return rokProdukcji;}
    

    public void setProducent(String producent)
    {this.producent= producent;}
	
	public void setRokProdukcji(LocalDate rokProdukcji)
    {this.rokProdukcji= rokProdukcji;}
    

    public abstract String dzwiek ();
	public boolean equals (Object a)
	{
		if (this==a)
		{
			return true;
		}
		if (a == null)
		{
			return false;
		}
		if (getClass() != a.getClass())
		{
			return false;
		}
		Instrument other= (Instrument)a;
		return rokProdukcji.equals(other.rokProdukcji) && producent.equals(other.producent);
	}
	
	public String toString()
	{return ("\nProducent: " + this.producent + "produkcja: " + rokProdukcji.toString() + ""); }
	private LocalDate rokProdukcji;
	private String producent;
}
