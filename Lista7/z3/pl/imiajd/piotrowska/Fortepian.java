package pl.imiajd.piotrowska;
import java.util.*;
import java.time.*;

public class Fortepian extends Instrument
{
	public Fortepian(String producent, int y, int m, int d)
	{super(producent,y,m,d);}
	
	public String dzwiek()
	{return ("brzdek fortepianu");}
	
	public boolean equals (Object a)
	{return super.equals(a);}
	
	public String toString()
	{return super.toString();}
}
