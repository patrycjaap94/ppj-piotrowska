import java.util.*;
import java.time.*;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];
		String[] imiona={"Bob","Krystian"};
		String[] imiona1={"Anna","Maria"};
        ludzie[0] = new Pracownik("Nowak",imiona,true,19894,1,13, 43117,2011,12,12);
        ludzie[1] = new Student("Iksinska",imiona1,false,1984,9,23, "socjologia",3.16);
        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko()+", "+p.getImiona()+", "+p.getdataUrodzenia()+", "+p.getPlec() + ": " + p.getOpis());
            if(p instanceof Pracownik){
				System.out.println("Pieniazki :"+ ((Pracownik) p).getPobory() +", Zatrudniony(a): "+ ((Pracownik) p).getdataZatrudnienia());
				}
			if(p instanceof Student){
				System.out.println("srednia:"+ ((Student) p).getSrednia() );}
        }
        ((Student) ludzie[1]).setSrednia(2.78);
        System.out.println("srednia:"+ ((Student) ludzie[1]).getSrednia() );
   }}

abstract class Osoba
{
    public Osoba(String nazwisko,String[] imiona,boolean plec,int rok,int miesiac,int dzien)
    {
        this.nazwisko = nazwisko;
        this.imiona=new String[2];
        this.imiona[0]=imiona[0];
        this.imiona[1]=imiona[1];
		
        this.dataUrodzenia=LocalDate.of(rok,miesiac,dzien);
		
			this.plec=plec;
        
        
        
    }

	
	
	
    public abstract String getOpis();

    public String getNazwisko()
    { return nazwisko;}
	
        public String[] getImiona()
    { return imiona;}
        public LocalDate getdataUrodzenia()
    { return dataUrodzenia;}
        public boolean getPlec()
    { return plec;}

    private String nazwisko;
    private String[] imiona;
    private boolean plec;    
	private LocalDate dataUrodzenia;
}
class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,String[] imiona,boolean plec,int rok,int miesiac,int dzien,double pobory,int rokz,int miesiacz,int dzienz)
    {super(nazwisko,imiona,plec,rok,miesiac,dzien);
       
        this.dataZatrudnienia=LocalDate.of(rokz,miesiacz,dzienz); 
		this.pobory = pobory;
    }

    public double getPobory()
    { return pobory;}
        public LocalDate getdataZatrudnienia()
    {return dataZatrudnienia;}

    public String getOpis()
    {return String.format("Pracownik otrzymuje pensje %.2f złotych", pobory);}

   
    private LocalDate dataZatrudnienia; 
	private double pobory;
}

class Student extends Osoba
{
    public Student(String nazwisko,String[] imiona,boolean plec,int rok,int miesiac,int dzien, String kierunek,double sredniaOcen)
    {
    super(nazwisko,imiona,plec,rok,miesiac,dzien);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {return "kierunek: " + kierunek;}
        public double getSrednia()
    {return sredniaOcen; }
   
    public void setSrednia(double srednia)
	{sredniaOcen=srednia;
		}
    private String kierunek;
    private double sredniaOcen;
}

