import java.util.*;

public class Zadanie4 {

    public static void main(String[] args)
    {

        Osoba o1 = new Osoba();
        System.out.print(o1.ToString());
        System.out.println();
        Student s1 = new Student("Kowalski", 123, "socjologia");
        System.out.println();
        System.out.print(s1.ToString());
        
        

    }



}


class Osoba {



    private String nazwisko;
    private int RokUrodzenia = 0;

    Osoba()
    {
    this.nazwisko = "BRAK";
    }

    Osoba(String nazwisko, int RokUrodzenia)
    {

        this.nazwisko = nazwisko;
        this.RokUrodzenia = RokUrodzenia;

    }

    public int getRokUrodznia()
    {

        return RokUrodzenia;
    }

    public String getnazwisko()
    {

        return nazwisko;

    }

    public String ToString()
    {

        return "\nNazwisko: " + nazwisko+"\nRok Urodzenia: "+RokUrodzenia;

    }

}


class Student extends Osoba {


    private String kierunek;

    Student(String nazwisko, int RokUrodzenia, String kierunek)
    {
        super(nazwisko, RokUrodzenia);
        this.kierunek = kierunek;
    }
   
   
   public String ToString()
    {

        return super.ToString()+"\nKierunek: "+kierunek;

    }



}


class Nauczyciel extends Osoba {

    private float pensja;

    Nauczyciel(String nazwisko, int RokUrodzenia, float pensja)
    {

        super(nazwisko, RokUrodzenia);
        this.pensja = pensja;

    }


}
